import java.time.*;
import java.time.chrono.ChronoZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

public class HistoryTeacher {

    public static void main(String[] args) {
        //Lincoln's metrics
        LocalDate now, bDateLincoln, dDateLincoln;
        now = LocalDate.now();
        bDateLincoln = LocalDate.of(1809, 2, 12);
        dDateLincoln = LocalDate.of(1855, 4, 15);
        System.out.println("He was: " + Period.between(bDateLincoln, dDateLincoln).getYears() + " years");
        System.out.println("He lived: " + bDateLincoln.until(dDateLincoln, ChronoUnit.DAYS) + " days");
        System.out.println();
        //Cumberbatch's metrics
        LocalDate bDateBennedict, dDateBennedict;
        bDateBennedict = LocalDate.of(1976, 7, 19);
        if (bDateBennedict.isLeapYear()) {
            System.out.println("He was born in a leap year");
        } else {
            System.out.println("He was born not in a leap year");
        }
        System.out.println("In that year was " + bDateBennedict.lengthOfYear() + " days");
        System.out.println("He is " + bDateBennedict.until(now, ChronoUnit.YEARS)+ " years now");
        System.out.println("It was " + bDateBennedict.plusYears(21).getDayOfWeek());
        System.out.println();

        //Train metrics
        LocalTime departs = LocalTime.of(1,45);
        LocalTime arrived = LocalTime.of(7,25);
        System.out.println("Duration of the trip is " + departs.until(arrived, ChronoUnit.MINUTES) + " minutes");
        System.out.println("Time of the arrived if the train was delayed will be " + arrived.plusHours(1).plusMinutes(19));
        System.out.println();

        //Flight metrics
        LocalDateTime flightTIme = LocalDate.of(2021, Month.MARCH, 24).atTime(9,15);
        LocalTime durationMiami = LocalTime.of(4,15);
        LocalDateTime arrivedTime = flightTIme.plusHours(durationMiami.getHour()).plusHours(12).plusMinutes(durationMiami.getMinute());
        System.out.println("Plane is arrived at " + arrivedTime);
        flightTIme = flightTIme.plusHours(4).plusMinutes(27);
        arrivedTime = flightTIme.plusHours(durationMiami.getHour()).plusHours(12).plusMinutes(durationMiami.getMinute());
        System.out.println("Plane is arrived at " + arrivedTime);
        System.out.println();

        //School metrics
        LocalDate todaySeptember = LocalDate.of(LocalDate.now().getYear(), Month.SEPTEMBER, LocalDate.now().getDayOfMonth());
        LocalDate semesterBegins = todaySeptember.with(TemporalAdjusters.dayOfWeekInMonth(2, DayOfWeek.TUESDAY));
        System.out.println("Begin: " + semesterBegins);
        LocalDate semesterEnds = LocalDate.of(semesterBegins.with(TemporalAdjusters.firstDayOfNextYear()).getYear(), Month.JUNE, 25);
        System.out.println("Ends: " + semesterEnds);
        long daysInSemester = semesterBegins.until(semesterEnds, ChronoUnit.DAYS) - semesterBegins.until(semesterEnds, ChronoUnit.WEEKS)*2-10*2;
        System.out.println("Semester duration is " + daysInSemester + " days");
        System.out.println();

        //Zoom meeting
        LocalDate now_ = LocalDate.now();
        LocalDateTime meetingTime = now_.with(TemporalAdjusters.next(DayOfWeek.TUESDAY)).atTime(1,30).plusHours(12);
        LocalDateTime previousTime = meetingTime.minusDays(7);
        System.out.println(previousTime);
        System.out.println();

        // ===== Task 2 ===== //

        ZoneId zoneBOS = ZoneId.of("America/New_York");
        ZoneId zoneSFO = ZoneId.of("America/Los_Angeles");
        ZoneId zoneBLR = ZoneId.of("Asia/Calcutta");
        zoneBLR = zoneBLR.normalized();
        LocalTime duration = LocalTime.of(5,30);

        //San Francisco to  Boston
        LocalDateTime flight_123 = LocalDate.of(2014, Month.JUNE, 13).atTime(10, 30).plusHours(12);
        ZonedDateTime flight_123SFO = ZonedDateTime.of(flight_123, zoneSFO);
        ZonedDateTime flight_123BOS = flight_123SFO.withZoneSameInstant(zoneBOS);
        ZonedDateTime flight_123BOSArrived = flight_123SFO.withZoneSameInstant(zoneBOS).plusHours(duration.getHour()).plusMinutes(duration.getMinute());
        ZonedDateTime flight_123SFOArrived = flight_123SFO.plusHours(duration.getHour()).plusMinutes(duration.getMinute());


        System.out.println("In Boston in that moment is " + flight_123BOS);
        System.out.println("In Boston when the plane is arrived is " + flight_123BOSArrived);
        System.out.println("In San Francisco when the plane is arrived is " + flight_123SFOArrived);
        System.out.println();

        LocalDateTime flight_456 = LocalDate.of(2014, Month.JUNE, 28).atTime(10, 30).plusHours(12);
        ZonedDateTime flight_456SFO = ZonedDateTime.of(flight_456, zoneSFO);
        LocalTime duration2 = LocalTime.of(22, 0);
        ZonedDateTime flight_456BLRArrived = flight_456SFO.withZoneSameInstant(zoneBLR).plusHours(duration2.getHour()).plusMinutes(duration2.getMinute());
        System.out.println("In Bangalore when the plane is arrived is " + flight_456BLRArrived.getDayOfWeek() + " " + flight_456BLRArrived);
        if (flight_456BLRArrived.getHour() == 9) {
            System.out.println("Yes, she will make a meeting!");
        } else {
            System.out.println("No, she won't make a meeting!");
        }
        ZonedDateTime flight_456SFOArrived = flight_456SFO.plusHours(duration2.getHour()).plusMinutes(duration2.getMinute());
        System.out.println("What time will be in SFO?" + flight_456SFOArrived);
        System.out.println();

        LocalDateTime flight_123_2 = LocalDate.of(2014, Month.NOVEMBER, 1).atTime(10, 30).plusHours(12);
        LocalTime duration3 = LocalTime.of(5, 30);
        ZonedDateTime flight_123_2_SFO = ZonedDateTime.of(flight_123_2, zoneSFO);
        ZonedDateTime flight_123_2BOSArrived = flight_123_2_SFO.withZoneSameInstant(zoneBOS).plusHours(duration3.getHour()).plusMinutes(duration3.getMinute());
        System.out.println("In Boston when the plane is arrived is " + flight_123_2BOSArrived.getDayOfWeek() + " " + flight_123_2BOSArrived);















    }
}
