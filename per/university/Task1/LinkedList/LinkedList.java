package per.university;

public class LinkedList {
    /**
     * An Embedded class Node (I know, that is not  a good style)
     */
    public class Node {
        Object info;
        Node next;
        /**
         * Constructor
         * @param info - Date for Node
         */
        public Node(Object info) { // Constructor - the creator of node!
            this.info = info;
            next = null;
        }
    }

    private Node head;
    /**
     * This Method adds a new Node in the end of current LinkedList
     * @param data - Data for New Node in LinkedList
     */
    public void Add (Object data) {
        if (head == null) {
            head = new Node(data);
            return;
        }
        else {
            Node current = head;
            while(current.next != null) {
                current = current.next;
            }
            current.next = new Node(data);
        }
    }
    /**
     * This Method deletes a special Node from LinkedList
     * @param data - Special identifier for deleting Node from LinkedList
     * @return Node - Deleting Node, if it exists in LinkedList
     */
    public Node Remove (Object data) {
        if (isEmpty()) {
            return null;
        }
        Node removable;
        Node current = head;
        while (current.next != null) {
            if (current.next.info == data) {
                removable = current.next;
                current.next = current.next.next;
                return removable;
            }
            else current = current.next;
        }
        return null;
    }
    /**
     * This Method adds a Node from LinkedList in special Order, which defined in parameters
     * If this Order isn't exists in current LinkedList The Method adds the Node in the end of LinkedList
     * @param data - Data of new Node in LinkedList
     * @param order - Place, where new Node will be added
     */
    public void addOrd (Object data, int order) {
        int counter = 0;
        Node current = this.head;
        Node previous = this.head;
        while (current != null) {
            if ((counter+1) == order) {
                Node internal = new Node(data);
                internal.next = current;
                previous.next = internal;
                return;
            }
            else {
                previous = current;
                current = current.next;
            }
            counter++;
        }
        Add(data);
        System.out.println("It is too large index, we add it item in the end of list or create new!");
    }
    /**
     * This Method returns a Special Node, which the User requires
     * @param order - the Node order
     * @return Node - Needed Node
     */
    public Node Get (int order) {
        int counter = 0;
        Node current = this.head;
        while (current != null) {
            if ((counter+1) == order) {
                System.out.print("This is your node :: ");
                System.out.println(current.info);
                return current;
            }
            else current = current.next;
            counter++;
        }
        System.out.println("At this place in linked list is infinity. Please check you choosing!");
        return null;
    }
    /**
     * This Method returns an Order of Node
     * @param data Data, which Order we want to check
     * @return Int - Order in all chain (LinkedList)
     */
    public int orderOf (Object data) {
        Node current = head;
        int counter = 0;
        while (current != null) {
            if (current.info == data) {
                System.out.print("The order of this element :: ");
                System.out.println(counter+1);
                return (counter+1);
            }
            else  {
                current = current.next;
                counter++;
            }
        }
        System.out.println("There isn't element with this value!");
        return -1;
    }
    /**
     * The Method check having Node of current LinkedList
     * @param data - Node value, which we want to check
     * @return true / false - Containing state
     */
    public boolean Contain (Object data) {
        Node current = this.head;
        while (current != null) {
            if (current.info == data) {
                System.out.println("Contain this element :)");
                return true;
            } else current = current.next;
        }
        System.out.println("Not contain this element :(");
        return false;
    }
    /**
     * This Method replaced two Nodes between them
     * @param data - Value for New Node in List
     * @param order - Order, where New Node will be putted
     * @return Node, which was replaced
     */
    public Node Set(Object data, int order) {
        Node current = this.head;
        int counter = 0;
        while (current != null) {
            if ((counter + 1) == order) {
                current.info = data;
                return current;
            }
            current = current.next;
            counter++;
        }
        System.out.println("I can't change this element :(");
        return null;
    }
    /**
     * This Method returns a total number of all Node in LinkedList
     * @return Int - Amount of all Nodes
     */
    public int Size () {
        Node current = head;
        int size = 0;
        while (current != null) {
            size++;
            current = current.next;
        }
        System.out.print("SIZE :: ");
        System.out.println(size);
        return size;
    }
    /**
     * This Method checks if current LickedList is Empty now
     * @return true / false - is Empty state
     */
    public boolean isEmpty () {
        return ((head == null) ? (true) : (false));
    }
    public void Show () {
        Node current = this.head;
        while (current != null) {
            System.out.print(current.info);
            current = current.next;
            if (current != null) {
                System.out.print(" --> ");
            }
        }
        System.out.println();
    }
    /**
     * This Method deletes the LinkedList (I thought that scavenger checks all)
     */
    public void Delete () {
        this.head = null;
    }
}
