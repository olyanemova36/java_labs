import org.junit.*;
import org.junit.rules.ExpectedException;

import java.time.LocalDate;
import per.university.RTree;
import per.university.Node;
import per.university.linkedList;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
public class RTreeTest {
    private RTree woody;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void globalSetUp() {
        System.out.println("Initial setup...");
        System.out.println("Code executes only once");
    }

    @Before
    public void setUp() {
        System.out.println("Code executes before each test method");
        Node son = new Node (1);
        Node son1 = new Node (2);// Works good
        Node son2 = new Node (3);
        woody = new RTree(6, 3);
        woody.addChild(son);
        woody.addChild(son1);
        woody.addChild(son2);
    }

    @Test
    public void checkTheSizeOfTree() throws Exception {
        assertThat(woody.root.Size(3), is(4));
    }

    @Test
    public void whenRemoveChild(){
        woody.removeChild(2);
        assertThat(woody.root.Size(3), is(3));
    }

    @Test
    public void getPath(){
        int size = woody.root.toList(3).Size();
        assertThat(4, is (size));
    }

    @AfterClass
    public static void tearDown() {
        System.out.println("Tests finished");
    }

    @After
    public void afterMethod() {
        System.out.println("Code executes after each test method");
    }


}
