package per.university;

public class Main {
    public static void main(String[] args) {
        RTree wood = new RTree(5,3);// Works good
        Node son = new Node (1);
        Node son1 = new Node (2);// Works good
        Node son2 = new Node (3);

        wood.addChild(son);
        wood.addChild(son1);
        wood.addChild(son2);// Works good

        if (wood.root.getParent() != null) {
            System.out.println(wood.root.getParent().value);
        }
        else {
            System.out.println("This is the root!");
        }

        if (wood.root.childs[2].Path() != null) {
            wood.root.childs[2].Path().show();
        }

        linkedList listy = new linkedList();
        if (wood.root.toList(wood.r) != null) {
            wood.root.toList(wood.r).show();
        }
        else {
            System.out.println("Thee is empty!");
        }

        if (wood.root.childs[0].findParent(wood.root.childs[1]) != null) {
            System.out.println(wood.root.childs[0].findParent(wood.root.childs[1]).value);
        }

        System.out.println(wood.root.Size(3));
        wood.removeChild(1); // Works good
        System.out.println(wood.root.Size(3)); // Works good
        if (wood.isEmpty()) { // Works good
            System.out.println("Three is empty!");
        }
        else {
            System.out.println("Three is not empty!");
        }

    }
}
