package per.university;

public class Node {

    Node parent; /** Parent of Node*/
    Object value; /** Data*/
    Node[] childs; /** All children of current Node*/
    /**
     * Constructor - Create a new Class object
     * @param data - Value with its Node data
     */
    public Node(Object data, int r) {
        this.value = data;
        this.parent = null;
        this.childs = new Node[r];
    }
    public Node(Object data) {
        this.value = data;
        this.parent = null;
        this.childs = null;
    }

    /**
     * This Method compares two different children from current Tree
     * @param other - It's sibling
     * @return true / false - Comparing two Nodes state
     */
    public boolean Compare (Node other){ //Ready
        if (this == null) {
            return false;
        }
        if (other == null) {
            return false;
        }
        else {
            return this.equals(other);
        }
    }



    /**
     * This Method returns a current node parent
     * @return this.parent - parent (especially if it exists)
     */
    public Node getParent() { //Ready
        return this.parent;
    }

    /**
     *This Method finds a common parent between two different children
     * @param another - Some sibling
     * @return ptr.parent / null - Common parent / them doesn't nave a common parent
     */
    public Node findParent(Node another) {
        if (another == null) {
            return null;
        }
        if (this.parent!=null) {
            Node current = this;
            while (current != null) {
                Node ptr = another;
                while (ptr != null) {
                    if (current.parent.Compare(ptr.parent)) {
                        return ptr.parent;
                    }
                    ptr = ptr.parent;
                }
                current = current.parent;
            }

        }
        return null;
    }

    /**
     * This Method returns a total count of all nodes in current tree
     * @return int amount - Total number of all children in this tree
     */
    public int Size(int r) { //Ready
        if (this == null) {
            return 0;
        }
        int amount = 1;
        Node current = this;
        int i = 0;
        while (i < r) {
            if (current.childs != null) {
                if (current.childs[i] != null) {
                    amount = amount + current.childs[i].Size(r);
                }
            }
            else {
                return 1;
            }
            i++;
        }
        return amount;
    }

    /**
     *This Method returns a path from current child to his main parent, which parent is null
     * @return LinkedList Path - Path from current child to his main parent(root)
     */
    public linkedList Path() { //Ready
        if (this == null) {
            return null;
        }
        Node current = this;
        linkedList path = new linkedList(current.value);
        while (current.parent != null) {
            path.Add(current.parent.value);
            current = current.parent;
        }
        return path;
    }

    /**
     * This Method creates a LinkedList from R-tree
     * @return wood - Result LinkedList from current R-tree
     */
    public linkedList toList(int r) { // Ready
        if (this == null) {
            return null;
        }
        linkedList wood = new linkedList(this.value);
        Node ptr = this;

        int i = 0;
        while (i < r) {
            if (ptr.childs != null) {
                if (ptr.childs[i]!= null) {
                    wood.Add(ptr.childs[i].value);
                }
            }
            else {
                wood.Add(ptr.value);
            }
            i++;
        }
        return wood;
    }
    /**
     * This Method finds needed Node in all Tree in value terms
     * @param value - Which Node we need, declared in value parameter
     * @return Node - Needed Node, if it exists
     */
    public Node Find(Node value, int r) { //Ready

        if (value == null) {
            return null;
        }
        Node current = this;
        int i = 0;
        while (i < r) {
            if (current.childs != null) {
                if (current.childs[i] != null) {
                    current.childs[i].Find(value, r);
                }
            }
            else {
                if (current.Compare(value)) {
                    return current.childs[i];
                }
            }
            i++;
        }
        return null;
    }
}
