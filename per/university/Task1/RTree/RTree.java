package per.university;

public class RTree {
    public
    Node root;
    int r;

    /**
     * Constructor
     * @param r - Date for root Node
     */
    public RTree(int r) {
        this.root = null;
        this.r = r;
    }
    public RTree(Object data, int r) {
        this.root = new Node(data, r);
        this.r = r;
    }
    /**
     * This Method adds a new child in array of all current node children
     * @param value - Value of adding child
     */
    public void addChild(Node value) { // Requirsize
        Node current = this.root;
        Node prev = null;
        if (current == null) {
            this.root = new Node(null, this.r);
            return;
        }
        if (current.childs != null) {
            int i = 0;
            for(i = 0; i < this.r; i++) {
                if (current.childs[i] == null) {
                    break;
                }
            }
            current.childs[i] = value;
            current.childs[i].parent = current;
        }
        else {
            assert false;
            current.childs = new Node[r];
            current.childs[0] = value;
            current.childs[0].parent = current;
        }
    }
    /** Overloading the function
     * This method removes a demand child with special value from all children
     * @param value - Value of child that you want to delete
     * @return true / false - Remove executing state
     */
    public boolean removeChild(Node value) { // Ready
        if (this.root == null) {
            return false;
        }
        if (value == null) {
            return false;
        }
        int i = 0;
        Node current = this.root;
        while (i < this.r) {
            if (current.childs[i].Compare(value)) {
                current.childs[i].parent = null;
                while (i < this.r) {
                    if (i+1 == this.r) {
                        current.childs[i] = null;
                        return true;
                    }
                    current.childs[i] = current.childs[i + 1];
                    i++;
                }
                return true;
            }
            i++;
        }
        return false;
    }
    /** Overloading the function
     * This method removes a demand child in order from all children
     * @param index - Order of child in all array of children
     * @return true/false - Remove executing state
     */
    public boolean removeChild(int index) { // begin from 0
        if (this.root == null) {
            return false;
        }
        Node current = this.root;

        if ( index > this.r) {return false; }
        if (current.childs[index-1] == null) { return false; }

        current.childs[index-1].parent = null;
        while ((index-1) < this.r) {
            if ((index-1) == ((this.r)-1)) {
                current.childs[index-1] = null;
                return true;
            }
            current.childs[index-1] = current.childs[index];
            index++;
        }
        return true;
    }


    /**
     * This method checks an empty state of root Node
     * @return true / false - isEmpty state
     */
    public boolean isEmpty() {
        return root == null;
    }

}
