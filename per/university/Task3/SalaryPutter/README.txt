You need to check this:
-----> MainTestInterfaces.java (With lambda operators)

Task:
    Для своих реализованных классов Employee в лабораторной работе:

    Создать и использовать Consumer Lambda Expression - OK
    Создать и использовать Function Lambda Expression - OK
    Создать и использовать Supplier Lambda Expression - OK
    Создать и использовать BiPredicate Lambda Expression - OK

    Кейсы необходимо придумать самостоятельно, главное, чтобы были реализованы данные функциональные интерфейсы

Were created:
  1.    // GrowUp (Up level in company)
        Consumer <Employee> promotion;

        // GrowUp (Up Employee Age)
        Consumer <Employee> growUp;

  2.    /** Supplier Lambda Expression*/
        // Get vacation Days
        Supplier<Integer> getVacationDays;

  3.    /** Function Lambda Expression*/
        // Returns string : "You get paid today : .... $"
        Function <Employee, String> getPaid;

  4.    /** Function Lambda Expression*/
        // Who is the best?
        BiPredicate <Employee, Employee > iAmBetter;

You need to check this:
Main.java (With new stram methods)
      На основании прошлой лабораторной работы добавить Employee поле salary (зарплата) - OK
      и cоздать по 2 примера использования следующих методов стримов:
      1. map и peek - ОК
      2. findFirst и lazy операции - OК
      3. max, min - OK
      4. average, sum - ОК
