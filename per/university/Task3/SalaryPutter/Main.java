package com.university;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        Accountant secretary = new Accountant();
        List<Employee> eList = Employee.createShortList();
        //Выплата премии женщинам сотрудникам
        assert false;
        System.out.println("--Salary for Female employee--");
        eList.stream()
                .filter((e) -> e.getGender().equals(GenderType.FEMALE))
                .forEach(secretary::payPremium);
        //Выплата зарплаты сотрудникам определенного департамента - Researching
        System.out.println("--Salary for special Department employee--");
        eList.stream()
                .filter((e) -> e.getDept().equals("Researching"))
                .forEach(secretary::paySalary);
        //Выплата премии сотрудникам старше 30, работающим в определенном департаменте
        System.out.println("--Salary for special Department employee elder then 30--");
        eList.stream()
                .filter((e) -> e.getDept().equals("Researching"))
                .filter((e) -> e.getAge() > 30)
                .forEach(secretary::paySalary);
        //Выплата зарплаты менеджерам
        System.out.println("--Salary for MANAGER employee--");
        eList.stream()
                .filter((e) -> e.getRole().equals(RoleType.MANAGER))
                .forEach(secretary::paySalary);
        //Выплата премии стаффу
        System.out.println("--Salary for STAFF employee--");
        eList.stream()
                .filter((e) -> e.getRole().equals(RoleType.STAFF))
                .forEach(secretary::payPremium);

        eList.stream()
                .peek(bg -> {
                    System.out.print("The salary will like ");
                    System.out.println(bg);
                });
        //---------------------------------
        // Лабораторная № 4
        //---------------------------------
        //Показывет печать данных для фамилий -- Lazy
        eList.stream()
                .filter(e -> e.getSurName().length() > 5)
                .peek(e -> System.out.println("Employee at start: " + e.getSurName()))
                .collect(Collectors.toList()).stream()
                .filter(e -> e.getRole().equals(RoleType.MANAGER))
                .peek(e -> System.out.println("Employee of post filtering: " + e.getSurName()))
                .collect(Collectors.toList());

        System.out.println();

        //Можно составлять короткий текст для описания биографии сотрудника
        Stream<Employee> newStream = eList.stream()
                .filter(e -> e.getSurName().length() > 5)
                .peek(e -> System.out.println("Employee at start: " + e.getSurName()))
                .filter(e -> e.getRole().equals(RoleType.MANAGER))
                .peek(e -> System.out.println("Employee of post filtering: " + e.getSurName()));

        newStream
                .filter(e -> e.getSurName().equals("Roberts"))
                .peek(e -> System.out.println("Employee of postpost filtering: " + e.getSurName()))
                .collect(Collectors.toList());

        System.out.println();

        //Показвать сотрудника, кто первым получит зарплату - Иначе первым получит зарплату Harry Hacker
        Employee first = eList.stream()
                .filter(e -> e.getGender().equals(GenderType.MALE))
                .findFirst()
                .orElse(new Employee());
        System.out.println(first.getGivenName() + " " + first.getSurName() + " is first who get salary " + first.getSalary() + " $");

        System.out.println();

        //Показать у какого сотрудника самая большая зараплата
        int max = eList.stream()
                .mapToInt(Employee::getSalary)
                .max()
                .getAsInt();
        eList.forEach(e -> System.out.println(e.getGivenName() + " " + e.getSurName() + " earns the biggest salary in company " + max + "$"));

        System.out.println();

        //Показать у какого сотрудника самая низкая зараплата
        int min = eList.stream()
                        .mapToInt(Employee::getSalary)
                        .min()
                        .getAsInt();
        eList.forEach(e -> System.out.println(e.getGivenName() + " " + e.getSurName() + " earns the smallest salary in company " + min + "$"));

        double averageSalary = eList.stream()
                .mapToInt(Employee::getSalary)
                .average()
                .getAsDouble();
        System.out.println("The Average salary in company is " + averageSalary + " $");

        System.out.println();

        //Процентная близость к средней зарплате в компании
        double average = eList.stream()
                .mapToInt(Employee::getSalary)
                .average()
                .getAsDouble();

        eList.forEach(e -> System.out.println(e.getGivenName() + " " + e.getSurName() + " has dispersion of average  for " + (1 + (double) (e.getSalary() - average) / average) * 100
                        + " %"));

        System.out.println();

        //Суммарная зараплата в компании
        System.out.println("Tha ratio of member in whole company money");
        double sum = eList.stream()
                .mapToInt(Employee::getSalary)
                .sum();
        eList.forEach(e -> System.out.println(e.getGivenName() + " " + e.getSurName() + " has ratio for " + ((double) e.getSalary() /sum ) * 100
                        + " %"));
    }
}
