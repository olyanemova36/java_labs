package com.university;

import org.jetbrains.annotations.NotNull;

public class Accountant {

    public void paySalary(@NotNull Employee employee) {
        //Выплату зарплаты реализовать через вывод в консоль сообщения о выплате
        System.out.println("Payment has been made for an employee:");
        String shortStory = employee.getSummary();
        System.out.printf("Salary:  %.0f\n", (employee.role.getSalary()));
        System.out.println(shortStory);
    }

    public void payPremium(@NotNull Employee employee) {
        //Выплату премии реализовать через вывод в консоль сообщения о выплате. Процент премии хранится в enum role (STAFF = 10%, MANAGER = 20%, EXECUTIVE = 30%)
        System.out.print("Premium payment has been made for an employee: ");
        String shortStory = employee.getSummary();
        System.out.printf("Premium:  %.0f\n", (employee.role.getPremium()/100)*Employee.defaultSalary);
        System.out.println(shortStory);
    }

}
