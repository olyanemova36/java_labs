package com.university;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

class EmployeePredicate {
    @Contract(pure = true)
    public static @NotNull Predicate <Employee> isAdultMale(){
        return p -> p.getAge() > 21 && p.getGender().equals(GenderType.MALE);
    }
    @Contract(pure = true)
    public static @NotNull Predicate <Employee> isAdultFemale() {
        return p -> p.getAge() > 18 && p.getGender().equals(GenderType.FEMALE);
    }
    @Contract(pure = true)
    public static @NotNull Predicate <Employee> isAgeMoreThan(int age) {
        return p -> p.getAge() > age;
    }
    public static List<Employee> filterEmployees(@NotNull List <Employee> employees, Predicate<Employee> predicate) {
        return employees.stream().filter(predicate).collect(Collectors.<Employee>toList());
    }
}
