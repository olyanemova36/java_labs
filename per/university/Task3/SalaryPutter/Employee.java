package com.university;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Employee {

    protected static int defaultSalary = 10000;

    private String givenName = null;
    private String surName = null;
    private int age = -1;

    private GenderType gender = null;
    private RoleType role = null;
    private String dept = null;


    private String eMail = null;
    private String phone = null;
    protected String address = null;
    private String state = null;
    private int code = -1;

    private int salary;

    public void setName(String name) { this.givenName = name; }
    public void setSurName(String secName) { this.surName = secName; }
    public void setAge(int age) { this.age = age; }
    public void setDept(String dept) { this.dept = dept;}

    /** Constructors*/
    public Employee (String name, String surName, int age, GenderType gen) {
        this.givenName = name;
        this.surName = surName;
        this.age = age;
        gender = gen;
    }
    public Employee (String name, String surName, int age, GenderType gen, RoleType role, String dept){
        this(name, surName, age, gen);
        this.role = role;
        this.dept = dept;
    }
    public Employee (String name, String surName, int age, GenderType gen, RoleType role, String dept,
                     String eMail, String phone) {
        this(name, surName, age, gen, role, dept);
        this.eMail = eMail;
        this.phone = phone;
    }

    public Employee () {
        givenName = "Harry";
        surName = "Hacker";
        age = 20;

        role = RoleType.STAFF;
        gender = GenderType.MALE;
        
        eMail = "somepost@mail.com";
        dept = "Researching";
        phone = "123-456-789";
        address = "USA, Main Road, 86B";
        state = "Montana";
        code = 6;

    }

    public Employee(EmployeeBuilder employeeBuilder) {
        if (employeeBuilder == null) {
            throw new IllegalArgumentException("Please provide employee builder to build employee object.");
        }
        if (employeeBuilder.age <= 0) {
            throw new IllegalArgumentException("Please provide valid employee number.");
        }
        if (employeeBuilder.givenName == null || employeeBuilder.givenName.trim().isEmpty()) {
            throw new IllegalArgumentException("Please provide employee name.");
        }
        this.givenName = employeeBuilder.givenName;
        this.surName = employeeBuilder.surName;
        this.age = employeeBuilder.age;
        this.gender = employeeBuilder.gender;
        this.role = employeeBuilder.role;
        this.dept = employeeBuilder.dept;
        this.eMail = employeeBuilder.eMail;
        this.phone = employeeBuilder.phone;
        this.address = employeeBuilder.address;
        this.state = employeeBuilder.state;
        this.code = employeeBuilder.code;
        this.salary = employeeBuilder.salary;
    }

    /** Getters*/
    public String getSurName() { return surName; }
    public String getGivenName() { return givenName;}
    public int getAge() { return age; }
    public String getEMail () {return eMail;}
    public String getDept() { return dept;}
    public String getPhone() { return phone;}
    public String getAddress () { return address;}
    public String getState () { return state;}
    public int getCode () { return code;}
    public GenderType getGender () { return gender;}
    public RoleType getRole () { return role;}
    public int getSalary() {return salary;};

    /** Methods --- Change smt*/

    public void changeRole(RoleType role) {
        this.role = role;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    /** Methods*/

    private boolean doQualityCheck() {
        return (givenName != null && !givenName.trim().isEmpty()) && (surName != null && !surName.trim().isEmpty())
                && (age > 0);
    }
    @NotNull
    @Override
    public String toString () {
        String info =
        "Employee: " + this.givenName + " " + this.surName + "\n" +
        "Age: " + this.age + "\n" +
        "Gender: " + this.gender + "\n" +
        "Company Info: " + this.role.getLevel() + "\n" + this.dept + "\n" +
                "Contacts: " +  this.eMail + "\n" +
                "          " + this.phone + "\n" +
                "          " + this.address + "\n" +
                "          " + this.state + "\n" +
                "CountryID:" + this.code + "\n";
        return info;
    }
    public String getSummary () {
        String info =
                "Employee: " + this.givenName + " " + this.surName + "\n" +
                        "Age: " + this.age + "\n" +
                        "Gender: " + this.gender + "\n" +
                        "Company Info: " + this.role.getLevel();
        return info;
    }

    public static @Nullable List <Employee> createShortList() {

        List <Employee> company = new ArrayList<>();
            company.add(
                     new EmployeeBuilder()
                             .createName("Jhonny")
                             .createSurName("Peters")
                             .createAge(18)
                             .createGender(GenderType.MALE)
                             .createRole(RoleType.STAFF)
                             .createDeptName( "Researching")
                             .createEMail("pochta@mail.com")
                             .createAddress("USA")
                             .createPhone("6858357-79867")
                             .createState("Montana")
                             .createCode(4)
                             .createSalary(11000)
                             .build()
              );
        company.add(
                new EmployeeBuilder()
                        .createName("Anna")
                        .createSurName("German")
                        .createAge(30)
                        .createGender(GenderType.FEMALE)
                        .createRole(RoleType.MANAGER)
                        .createDeptName( "Researching")
                        .createEMail("pochta@mail.com")
                        .createAddress("USA")
                        .createPhone("6858357-79867")
                        .createState("Montana")
                        .createSalary(14000)
                        .createCode(4)
                        .build()
        );
        company.add(
                new EmployeeBuilder()
                        .createName("Katy")
                        .createSurName("Holms")
                        .createAge(27)
                        .createGender(GenderType.FEMALE)
                        .createRole(RoleType.STAFF)
                        .createDeptName( "Researching")
                        .createEMail("pochta@mail.com")
                        .createAddress("USA")
                        .createPhone("6858357-79867")
                        .createState("Montana")
                        .createCode(4)
                        .createSalary(9000)
                        .build()
        );
        company.add(
                new EmployeeBuilder()
                        .createName("Peter")
                        .createSurName("Jonson")
                        .createAge(40)
                        .createGender(GenderType.MALE)
                        .createRole(RoleType.EXECUTIVE)
                        .createDeptName( "Researching")
                        .createEMail("pochta@mail.com")
                        .createAddress("USA")
                        .createPhone("6858357-79867")
                        .createState("Montana")
                        .createCode(4)
                        .createSalary(37000)
                        .build()
        );
        company.add(
                new EmployeeBuilder()
                        .createName("Mary")
                        .createSurName("Roberts")
                        .createAge(36)
                        .createGender(GenderType.FEMALE)
                        .createRole(RoleType.MANAGER)
                        .createDeptName( "Researching")
                        .createEMail("pochta@mail.com")
                        .createAddress("USA")
                        .createPhone("6858357-79867")
                        .createState("Montana")
                        .createSalary(15000)
                        .createCode(4)
                        .build()
        );
        company.add(
                new EmployeeBuilder()
                        .createName("Brad")
                        .createSurName("Holdberg")
                        .createAge(20)
                        .createGender(GenderType.MALE)
                        .createRole(RoleType.STAFF)
                        .createDeptName("Business")
                        .createEMail("pochta@mail.com")
                        .createAddress("USA")
                        .createPhone("6858357-79867")
                        .createState("Montana")
                        .createCode(4)
                        .createSalary(10000)
                        .build()
        );
        company.add(
                new EmployeeBuilder()
                        .createName("James")
                        .createSurName("Bond")
                        .createAge(40)
                        .createGender(GenderType.FEMALE)
                        .createRole(RoleType.STAFF)
                        .createDeptName("Business")
                        .createEMail("pochta@mail.com")
                        .createAddress("USA")
                        .createPhone("6858357-79867")
                        .createState("Montana")
                        .createCode(4)
                        .createSalary(10000)
                        .build()
        );

        return company;
    }

    public static class EmployeeBuilder {
        private String givenName;
        private String surName;
        private int age;
        private GenderType gender;
        private RoleType role;
        private String dept;

        private String eMail;
        private String phone;
        private String address;
        private String state;
        private int code;

        private int salary;

        public EmployeeBuilder() {
            super();
        }

        public EmployeeBuilder createName (String name) {
            this.givenName = name;
            return this;
        }
        public EmployeeBuilder createSurName(String surName) {
            this.surName = surName;
            return this;
        }
        public EmployeeBuilder createAge(int age) {
            this.age = age;
            return this;
        }
        public EmployeeBuilder createDeptName(String dept) {
            this.dept = dept;
            return this;
        }
        public EmployeeBuilder createGender(GenderType type) {
            this.gender = type;
            return this;
        }
        public EmployeeBuilder createRole(RoleType role) {
            this.role = role;
            return this;
        }
        public EmployeeBuilder createEMail(String eMail) {
            this.eMail = eMail;
            return this;
        }
        public EmployeeBuilder createPhone(String phone) {
            this.phone = phone;
            return this;
        }
        public EmployeeBuilder createAddress(String address) {
            this.address = address;
            return this;
        }
        public EmployeeBuilder createState(String state) {
            this.state = state;
            return this;
        }
        public EmployeeBuilder createCode(int code) {
            this.code = code;
            return this;
        }
        public EmployeeBuilder createSalary(int salary) {
            this.salary = salary;
            return this;
        }

        public Employee build() {
            Employee human = null;
            if (validateEmployee()) {
                human = new Employee(this);
            } else {
                System.out.println("Sorry! Employee objects can't be build without required details");
            }
            return human;
        }
        private boolean validateEmployee() {
            return (age > 0 && givenName != null && !givenName.trim().isEmpty());
        }

    }
}
