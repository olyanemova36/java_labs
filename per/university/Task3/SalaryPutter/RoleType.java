package com.university;

public enum RoleType {
    STAFF ("Staff", 10, Employee.defaultSalary),
    MANAGER ("Manager", 20, Employee.defaultSalary),
    EXECUTIVE ("Executive", 30,Employee.defaultSalary );

    private final int premium;
    private final int salary;
    private final String level;



    RoleType(String level, int premium, int salary) {
        this.premium = premium;
        this.salary = salary*(this.premium/10);
        this.level = level;
    };
    public double getPremium() {
        return this.premium;
    }
    public String getLevel() {
        return this.level;
    }
    public double getSalary() {
        return this.salary;
    }
}
