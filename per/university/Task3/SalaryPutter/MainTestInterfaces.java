package com.university;

import java.util.ArrayList;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class MainTestInterfaces {

    public static void main(String[] args) {

        /** Consumer Lambda Expression*/
        // Promotion (Up level in company)
        Consumer <Employee> promotion = employee -> employee.changeRole(
                employee.getRole().equals(RoleType.STAFF) ? RoleType.MANAGER : RoleType.EXECUTIVE
        );
        Employee employee = new Employee();
        System.out.println(employee.getSummary());
        promotion.accept(employee);
        System.out.println(employee.getSummary());

        // GrowUp (Up level in company)
        Consumer <Employee> growUp = employee1 -> employee1.setAge(
                employee1.getAge()+1
        );
        System.out.println(employee.getSummary());
        growUp.accept(employee);
        System.out.println(employee.getSummary());

        /** Supplier Lambda Expression*/
        // Get vacation Days
        Supplier<Integer> getVacationDays = () -> 14;
        System.out.println(getVacationDays.get());

        /** Function Lambda Expression*/
        // Returns string : "You get paid today : .... $"
        Function <Employee, String> getPaid = employee2 -> "You get paid today: " + employee2.getSalary() + "$";
        System.out.println(getPaid.apply(employee));

        /** Function Lambda Expression*/
        // Who is the best?
        BiPredicate <Employee, Employee > iAmBetter = ((employee3, employee4) ->  {
            if (employee3.getRole().equals(RoleType.STAFF)) {
                return false;
            }
            else if (employee3.getRole().equals(RoleType.EXECUTIVE)) {
                return true;
            }
            else return employee3.getRole().equals(RoleType.MANAGER) && employee4.getRole().equals(RoleType.STAFF);
        });

        Employee employee_ = new Employee();//Staff employee is Manager
        System.out.println(iAmBetter.test(employee, employee_));

    }
}
