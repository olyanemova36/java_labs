package personal.university;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Node {
    int info;
    Node right;
    Node left;
    /**
     * Constructor
     * @param info Data for node
     */
    public Node(int info) { // Constructor - the creator of node!
        this.info = info;
        this.right = null;
        this.left = null;
    }

    public void Push (int data) {
        Node elem = new Node(data);
        Node current = this;
        Node prev = null;
        while (current!= null){
            if (data > current.info) {
                prev = current;
                current = current.right;
            }
            else {
                prev = current;
                current = current.left;
            }
        }
        if (prev.info > data) {
            prev.left = elem;
        }
        else {
            prev.right =elem;
        }
    }

}