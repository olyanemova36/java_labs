package personal.university;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Wither;

@Builder
@Data
public class BTree {
    Node head;
}
