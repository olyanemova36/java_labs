package personal.university;
import lombok.NonNull;

public class Main {
    public static void main(String[] args) {

        Node leaf_1 = new Node(6);
        int leaf_2 = 8;
        int leaf_3 = 5;
        int leaf_4 = 7;
        int leaf_5 = 4;
        int leaf_6 = 10;
        int leaf_7 = 20;

        BTree wood = new BTree(leaf_1);
        wood.head.Push(leaf_2);
        wood.head.Push(leaf_3);
        wood.head.Push(leaf_4);
        wood.head.Push(leaf_5);
        wood.head.Push(leaf_6);
        wood.head.Push(leaf_7);
        wood.head.Push(30);
        wood.head.Push(1);
        wood.head.Push(40);

        System.out.println(DeepSearch(wood.head));
    }

    @NonNull
    public static int DeepSearch(Node head) {
        int l = head.left == null? 0 : DeepSearch(head.left);
        int d = head.right == null? 0 : DeepSearch(head.right);
        return 1 + Math.max(l, d);
    }
}