package personal.university;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    public static <TypeKey> void main(String[] args) {

        if (args == null) {
            System.exit(1);
        }
        HashSet<String> source = new HashSet<String>();
        int sum = 0;
        try {
            File file = new File(args[0]);
            Scanner myReader = new Scanner(file);
            while (myReader.hasNextLine()) {
                List<String> current = getTokens(myReader.nextLine());
                sum += current.size();
                for(String row:current) {
                    if (source.add(row)) {
                        source.add(row);
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        for (String name:source) {
            System.out.print(name + " " );
        }
        System.out.println(sum);
    }

    public static List<String> getTokens(String str) {
        List tokens = new ArrayList<>();
        StringTokenizer tokenizer = new StringTokenizer(str, " ");
        while (tokenizer.hasMoreElements()) {
            tokens.add(tokenizer.nextToken());
        }
        return tokens;
    }
}
